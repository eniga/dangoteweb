import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthenticationRequest, AuthenticationResponse, PasswordResetRequest, PasswordResetResponse, CustomerDetailsRequest, CustomerDetailsResponse, PostPaymentRequest, PostPaymentResponse, GetPriceRequest, GetPriceResponse, CreateAtcRequest, CreateAtcResponse, Areas, Records, Regions, Transactions, LoginRequest, LoginResponse, StaticData } from './models';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  constructor(private http: HttpClient) { }

  /* Authentication */

  Authenticate(request: AuthenticationRequest) {
    return this.http.post<AuthenticationResponse>(environment.api_url + "authentication", request);
  }

  PasswordReset(request: PasswordResetRequest) {
    return this.http.post<PasswordResetResponse>(environment.api_url + "authentication/passwordreset", request);
  }

  /* End Authentication */
  /* Customer */

  GetCustomerDetails(request: CustomerDetailsRequest) {
    return this.http.post<CustomerDetailsResponse>(environment.api_url + "GetCustomer", request);
  }

  /* End Customer */
  /* PostPayment */

  PostPayment(request: PostPaymentRequest) {
    return this.http.post<PostPaymentResponse>(environment.api_url + "PostPayment", request);
  }

  /* End PostPayment */
  /* GetPrice */

  GetPrice(request: GetPriceRequest) {
    return this.http.post<GetPriceResponse>(environment.api_url + "GetPrice", request);
  }

  /* End GetPrice */
  /* CreateAtc */

  CreateAtc(request: CreateAtcRequest) {
    return this.http.post<CreateAtcResponse>(environment.api_url + "CreateATC", request);
  }
  /* End CreateAtc */
  /* GetAreas */
  GetAreas() {
    return this.http.get<Areas[]>(environment.api_url + "data/areas");
  }
  /* End GetAreas */
  /* GetAreas */
  GetRegions() {
    return this.http.get<Regions[]>(environment.api_url + "data/regions");
  }
  /* End GetAreas */
  /* GetAreas */
  GetRecords() {
    return this.http.get<Records[]>(environment.api_url + "data/records");
  }
  /* End GetAreas */

  /* GetTransactions */
  GetTransactions(username: string) {
    return this.http.get<Transactions[]>(environment.api_url + "transaction/username/" + username);
  }
  /* End GetTransactions */
  /* GetTransactions */
  GetTransactionById(Id: string) {
    return this.http.get<Transactions>(environment.api_url + "transaction/" + Id);
  }
  /* End GetTransactions */
  /* Login */
  Login(request: LoginRequest) {
    return this.http.post<LoginResponse>(environment.api_url + "authentication/login", request);
  }
  /* End Login */

  /* Get Static Data */
  GetData = () => {
    return this.http.get<StaticData>(environment.api_url + "Data");
  }
}
