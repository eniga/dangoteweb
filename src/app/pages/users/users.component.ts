import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {NgbModal, ModalDismissReasons }from '@ng-bootstrap/ng-bootstrap';
import { ServicesService } from 'src/app/services.service';

declare const $: any;

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  closeResult: string;
  isEdit = false;
  temp = [];

  userId: number;
  userType: string;
  accountStatus: boolean;
  userCategory: string;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  regDate: Date;

  rows: any = [];

  loadingText = 'Loading...';

  constructor(public modalService: NgbModal, private ref: ChangeDetectorRef, private service: ServicesService) {
  }

  ngOnInit() {
    this.InitTable();
  }

  InitTable(){
    this.ref.detectChanges();

    $('#datatables').DataTable({
      destroy: true,
      'processing': true,
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();
  }

  onSelect($event) {
   console.log('Select Event', $event);
  }

  onAdd(email){
  }

  addFunction(){
  }

  edit(type, content){
    this.isEdit = true;
    this.userId = content.userID;
    this.userType = content.userType;
    this.accountStatus = content.accountStatus;
    this.userCategory = content.userCategory;
    this.firstName = content.firstName;
    this.lastName = content.lastName;
    this.email = content.email;
    this.phone = content.phone;
    this.regDate = content.regDate;
    this.modalService.open(type).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  editFunction(){
  }

  deleteFunction(id){
    this.temp = this.rows.filter(entry => entry.id !== id);
    return false;
  }
  

  open(content, type) {
    this.isEdit = false;
    
      this.modalService.open(content).result.then((result) => {
          this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
  }

  close(content){
    this.modalService.dismissAll(content);
  }

  public getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
          return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
          return 'by clicking on a backdrop';
      } else {
          return  `with: ${reason}`;
      }
  }
}
