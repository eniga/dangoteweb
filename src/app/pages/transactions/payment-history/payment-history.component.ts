import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ServicesService } from 'src/app/services.service';
import { Transactions } from 'src/app/models';
declare const $: any;

@Component({
  selector: 'app-payment-history',
  templateUrl: './payment-history.component.html',
  styleUrls: ['./payment-history.component.css']
})
export class PaymentHistoryComponent implements OnInit {

  temp = [];
  activeRow: any;
  rows: Transactions[] = [];
  loadingData: string = 'Loading...';
  username = localStorage.getItem('user');

  constructor(private ref: ChangeDetectorRef, private service: ServicesService) { }

  ngOnInit() {
    this.InitTable()
  }

  InitTable() {
    this.service.GetTransactions(this.username).subscribe((data) => {
      this.rows = data.sort((n1, n2) => {
        if (n1.transaction_date > n2.transaction_date) {
          return 1;
        }

        if (n1.transaction_date < n2.transaction_date) {
          return -1;
        }

        return 0;
      });

      this.ref.detectChanges();

      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      const table = $('#datatables').DataTable();
    })
  }
}
