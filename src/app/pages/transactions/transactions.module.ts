import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransactionsRoutingModule } from './transactions-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormsModule } from '@angular/forms';
import { CreateAtcComponent } from './create-atc/create-atc.component';
import { EWalletComponent } from './e-wallet/e-wallet.component';
import { PaymentHistoryComponent } from './payment-history/payment-history.component';
import { NgxBarcode6Module } from 'ngx-barcode6';

@NgModule({
  declarations: [CreateAtcComponent, EWalletComponent, PaymentHistoryComponent],
  imports: [
    CommonModule,
    TransactionsRoutingModule,
    NgxDatatableModule,
    FormsModule,
    NgxBarcode6Module
  ]
})
export class TransactionsModule { }
