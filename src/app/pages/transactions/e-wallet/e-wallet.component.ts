import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ServicesService } from 'src/app/services.service';
import { CustomerDetailsRequest, CustomerOutRespose, PostPaymentRequest, Areas, Regions, Records, BankPayment, StaticData } from 'src/app/models';
import * as moment from 'moment';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-e-wallet',
  templateUrl: './e-wallet.component.html',
  styleUrls: ['./e-wallet.component.css']
})
export class EWalletComponent implements OnInit {
  public loading: boolean;
  today = new Date;
  step = 1;
  cust_num = '';
  access_token = '';
  teller_no = '';
  bankn = environment.dangote.bankn;
  cust_number = '';
  amount = '';
  tran_id = Math.floor(1000000000 + Math.random() * 9000000000);
  currency = 'NGN';
  plant = '';
  material = '';
  materiallist: Records[] = [];
  cust_name = '';
  areas: Areas[] = [];
  regions: Regions[] = [];
  records: Records[] = [];
  dest_country = '';
  dest_region = '';
  account_no = '';

  // Response back to interface
  has_error = false;
  error_msg = '';
  username = localStorage.getItem('user');

  // Customer details response;
  custDetails: CustomerOutRespose;
  payment: BankPayment;
  staticData: StaticData;

  constructor(public modalService: NgbModal, private ref: ChangeDetectorRef, private service: ServicesService, private router: Router) { }

  ngOnInit() {
    this.GetData();
    this.getRecords();
    this.getRegions();
  }
  GetData = () => {
    this.service.GetData().subscribe(data => {
      this.staticData = data;
    })
  }  

  printpreview(){
    this.router.navigate(['../../pages/print-receipt/' + this.tran_id]);
  }

  getRecords(){
    this.service.GetRecords().subscribe((data) => {
      this.records = data;
      // let codes = data.map(x => {return x.company_code});
      // this.comp_codes = codes.filter((v, i, a) => a.indexOf(v) === i);
      this.materiallist = this.records;
    });
  }

  OnSelectedRegion($event){
    let country = this.regions.filter(x => x.country_code === this.dest_region);
    this.dest_country = country[0].country;
  }

  OnSelectedPlant($event){
    this.materiallist = this.records.filter(x => x.plant === this.plant);
  }

  getRegions() {
    this.service.GetRegions().subscribe((data) => {
      this.regions = data;
    });
  }

  setPage(step: number) {
    this.step = step;
  }

  validateCustomer() {
    this.loading = true;
    this.has_error = false;

    let request = new CustomerDetailsRequest;
    request.cust_num = this.cust_num;
    request.username = this.username;
    this.service.GetCustomerDetails(request).subscribe((data) => {
      this.loading = false;
      if (data.mt_atm_cust_out.cust_nameField.length < 1) {
        this.has_error = true;
        this.error_msg = 'Invalid Customer Detail';
        return false;
      } else {
        this.has_error = false;
        this.error_msg = '';
        this.cust_name = data.mt_atm_cust_out.cust_nameField;
        this.custDetails = data.mt_atm_cust_out;
        this.cust_number = this.cust_num;
        this.setPage(2);
      }
    }, error => {
      this.loading = false;
      this.has_error = true;
      this.error_msg = error.statusText;
    });
  }

  postPayment() {
    this.loading = true;
    let request = new PostPaymentRequest;
    request.access_token = this.access_token;
    request.amount = this.amount;
    request.bankn = this.bankn;
    request.comp_code = this.staticData.comp_code;
    request.currency = this.currency;
    request.cust_number = this.cust_number;
    request.plant = this.plant;
    request.teller_no = this.teller_no;
    request.tran_id = this.tran_id.toString();
    request.username = this.username;
    request.cust_name = this.custDetails.cust_nameField;
    request.account_no = this.account_no;
    this.service.PostPayment(request).subscribe((data) => {
      this.loading = false;
      console.log(data);
      if (data.mt_bank_out.eRRORField !== null) {
        return false;
      } else {
        this.payment = data.mt_bank_out;
        this.setPage(3);
        console.log(this.payment);
      }
    });
  }

  open(content, type) {

    this.modalService.open(content).result.then((result) => {
      // this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  close(content) {
    this.modalService.dismissAll(content);
  }

  public getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  print() {
    this.printElement(document.getElementById("printThis"), false, null);
    window.print();
  }

  printElement(elem, append, delimiter) {
    var domClone = elem.cloneNode(true);

    var $printSection = document.getElementById("section-to-print");

    if (!$printSection) {
      $printSection = document.createElement("div");
      $printSection.id = "section-to-print";
      document.body.appendChild($printSection);
    }

    if (append !== true) {
      $printSection.innerHTML = "";
    }

    else if (append === true) {
      if (typeof (delimiter) === "string") {
        $printSection.innerHTML += delimiter;
      }
      else if (typeof (delimiter) === "object") {
        $printSection.appendChild(delimiter);
      }
    }

    $printSection.appendChild(domClone);
  }
}
