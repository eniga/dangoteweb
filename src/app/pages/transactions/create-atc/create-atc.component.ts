import { Component, OnInit, ChangeDetectorRef, ɵConsole } from '@angular/core';
import { CustomerOutRespose, CustomerDetailsRequest, GetPriceRequest, Materials, Quantities, PriceResponse, CreateAtcRequest, AtcResponse, Areas, Regions, Records, Quantities2, StaticData } from 'src/app/models';
import { ServicesService } from 'src/app/services.service';
import {NgbModal, ModalDismissReasons }from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { ComponentsModule } from 'src/app/components/components.module';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-create-atc',
  templateUrl: './create-atc.component.html',
  styleUrls: ['./create-atc.component.css']
})
export class CreateAtcComponent implements OnInit {
  public loading: boolean;
  today = new Date;
  step = 1;
  cust_num = '';
  access_token = '';
  teller_no = '';
  cust_number = '';
  amount = '0';
  tran_id = Math.floor(1000000000 + Math.random() * 9000000000);
  currency = 'NGN';
  plant = '';
  material = '';
  materiallist: Records[] = [];
  materials: Materials[] = [];
  quantities: Quantities[] = [];
  qtyties: Quantities2[] = [];
  delv_status = '';
  street = '';
  export = '';
  dest_address = '';
  dest_city = '';
  dest_country = '';
  dest_region = '';
  order_amount = '0';
  receiver_name = '';
  split_quantities: Quantities[] = [];
  split_quantity = '';
  split_type = 'X';
  cust_name = '';
  areas: Areas[] = [];
  regions: Regions[] = [];
  records: Records[] =[];
  comp_codes = [];
  sales_orgs = [];
  quantity = '';
  account_no = '';
  bankn = environment.dangote.bankn;

  // Response back to interface
  has_error = false;
  error_msg = '';
  username = localStorage.getItem('user');

  // Customer details response;
  custDetails: CustomerOutRespose;
  priceDetails: PriceResponse;
  atcResponse: AtcResponse;
  staticData: StaticData;

  constructor(public modalService: NgbModal, private ref: ChangeDetectorRef, private service: ServicesService, private router: Router) { }

  ngOnInit() {
    this.GetData();
    this.getRecords();
    this.getRegions();
  }

  GetData = () => {
    this.service.GetData().subscribe(data => {
      this.staticData = data;
    })
  }

  printpreview(){
    this.router.navigate(['../../pages/print-receipt2/' + this.tran_id]);
  }

  getRecords(){
    this.service.GetRecords().subscribe((data) => {
      this.records = data;
      // let codes = data.map(x => {return x.company_code});
      // this.comp_codes = codes.filter((v, i, a) => a.indexOf(v) === i);
      let orgs = data.map(x => x.sales_organisation);
      this.sales_orgs = orgs.filter((v, i, a) => a.indexOf(v) === i);
      this.materiallist = this.records;
    });
  }

  OnSelectedRegion($event){
    let country = this.regions.filter(x => x.country_code === this.dest_region);
    this.dest_country = country[0].country;
  }

  OnSelectedPlant($event){
    this.materiallist = this.records.filter(x => x.plant === this.plant);
  }

  getRegions(){
    this.service.GetRegions().subscribe((data) => {
      this.regions = data;
    });
  }

  setPage(step: number){
    this.step = step;
  }

  validateCustomer(){
    // this.setPage(2);
    // return false;
    this.loading = true;
    this.has_error = false;
    if(this.cust_num.length < 1){
      this.has_error = true;
      this.error_msg = 'All fields are required';
      this.loading = false;
      return false;
    }
    let request = new CustomerDetailsRequest;
    request.cust_num = this.cust_num;
    request.username = this.username;
    this.service.GetCustomerDetails(request).subscribe((data) => {
      this.loading = false;
      if(data.mt_atm_cust_out.cust_nameField.length < 1){
        this.has_error = true;
        this.error_msg = 'Invalid Customer Detail';
        return false;
      } else {
        this.has_error = false;
        this.error_msg = '';
        this.cust_name = data.mt_atm_cust_out.cust_nameField;
        this.custDetails = data.mt_atm_cust_out;
        this.cust_number = this.cust_num;
        this.setPage(2);
      }
    }, error => {
      this.loading = false;
      this.has_error = true;
      this.error_msg = error.statusText;
    });
  }

  getPrice(){
    this.loading = true;
    this.has_error = false;
    let request = new GetPriceRequest;
    
    let record = this.records.filter(x => x.material ===  this.material);
    let mat = new Materials;
    mat.material_noField = record[0].material;
    mat.material_txtField = record[0].material_description;
    this.materials.push(mat);

    let qty = new Quantities2;
    qty.qtyField = +this.quantity;
    qty.qtyFieldSpeficied = true;
    this.qtyties.push(qty);

    request.acess_token = this.access_token;
    request.country = this.custDetails.countryField;
    request.cust_name = this.custDetails.cust_nameField;
    request.cust_num = this.cust_num;
    request.delv_status = this.delv_status;
    request.division = this.custDetails.divisionField;
    request.materials = this.materials;
    request.plant =  this.plant;
    request.quantities = this.qtyties;
    request.region = this.custDetails.regionField;
    request.sales_org = this.staticData.sales_org;
    request.street = this.custDetails.addressField;
    request.tran_id = this.tran_id.toString();
    request.username = this.username;
    this.service.GetPrice(request).subscribe((data) => {
      this.loading = false;
      if(data.mt_price_out.error_logField.length > 0){
        this.has_error = true;
        this.error_msg = data.mt_price_out.error_logField[0].messageField;
        return false;
      } else {
        this.has_error = false;
        this.error_msg = '';
        this.priceDetails = data.mt_price_out;
        this.order_amount = (+this.priceDetails.base_priceField + +this.priceDetails.delivery_priceField).toString();
        this.setPage(3);
      }
    }, error => {
      this.loading = false;
      this.has_error = true;
      this.error_msg = error.statusText;
    })
  }

  createAtc(){
    this.loading = true;
    this.has_error = false;

    let qty = new Quantities;
    qty.qtyField = +this.quantity;
    this.quantities.push(qty);
    let split = new Quantities;
    split.qtyField = +this.split_quantity;
    this.split_quantities.push(split);

    let request = new CreateAtcRequest;
    request.access_token = this.access_token;
    request.amount = this.order_amount;
    request.bankn = this.bankn;
    request.comp_code = this.staticData.comp_code;
    request.currency = this.currency;
    request.cust_num = this.cust_num;
    request.delv_status = this.delv_status;
    request.dest_address = this.dest_address;
    request.dest_city = this.dest_city;
    request.dest_country = this.dest_country;
    request.dest_region = this.dest_region;
    request.export = this.export;
    request.material = this.materials;
    request.order_amount = this.order_amount;
    request.plant = this.plant;
    request.quantity = this.quantities;
    request.receiver_name = this.receiver_name;
    request.sales_org = this.staticData.sales_org;
    request.split_quantity = this.split_quantities;
    request.split_type = this.split_type;
    request.teller_no = this.teller_no;
    request.tran_id = this.tran_id.toString();
    request.username = this.username;
    request.cust_name = this.custDetails.cust_nameField;
    request.account_no = this.account_no;
    console.log(request);
    this.service.CreateAtc(request).subscribe((data) => {
      console.log(data);
      console.log(data.mt_doc_out.errorField.length);
      this.loading = false;
      this.has_error = data.mt_doc_out.errorField.length > 0 ? true : false;
      
      if(this.has_error){
        this.error_msg = data.mt_doc_out.errorField[0].messageField;
        return false;
      } else {
        this.atcResponse = data.mt_doc_out;
        this.setPage(4);
      }
    }, error => {
      this.loading = false;
      this.has_error = true;
      this.error_msg = error.statusText;
    })
  }
}
