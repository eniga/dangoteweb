import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAtcComponent } from './create-atc.component';

describe('CreateAtcComponent', () => {
  let component: CreateAtcComponent;
  let fixture: ComponentFixture<CreateAtcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAtcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAtcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
