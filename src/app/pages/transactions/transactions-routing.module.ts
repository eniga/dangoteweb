import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EWalletComponent } from './e-wallet/e-wallet.component';
import { CreateAtcComponent } from './create-atc/create-atc.component';
import { PaymentHistoryComponent } from './payment-history/payment-history.component';

const routes: Routes = [
  {
    path: 'e-wallet',
    component: EWalletComponent
  },
  {
    path: 'create-atc',
    component: CreateAtcComponent
  },
  {
    path: 'payment-history',
    component: PaymentHistoryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransactionsRoutingModule { }
