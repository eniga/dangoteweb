import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {NgbModal, ModalDismissReasons }from '@ng-bootstrap/ng-bootstrap';
import { ServicesService } from 'src/app/services.service';

declare const $: any;

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  customers = [];
  rows: any = [];  
  closeResult: string;
  isEdit = false;


  loadingText = 'Loading...';

  constructor(public modalService: NgbModal, private ref: ChangeDetectorRef, private service: ServicesService) {}

  ngOnInit() {
    this.InitTable();
  }

  InitTable(){
    this.ref.detectChanges();

    $('#datatables').DataTable({
      destroy: true,
      'processing': true,
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();
  }
}
