import { Routes } from "@angular/router";

import { RegisterComponent } from "./register/register.component";
import { PricingComponent } from "./pricing/pricing.component";
import { LockComponent } from "./lock/lock.component";
import { LoginComponent } from "./login/login.component";
import { PrintReceiptComponent } from "./print-receipt/print-receipt.component";
import { PrintReceipt2Component } from "./print-receipt2/print-receipt2.component";

export const PagesRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "login",
        component: LoginComponent
      },
      {
        path: "lock",
        component: LockComponent
      },
      {
        path: "register",
        component: RegisterComponent
      },
      {
        path: "pricing",
        component: PricingComponent
      },
      {
        path: "print-receipt/:tran_id",
        component: PrintReceiptComponent
      },
      {
        path: "print-receipt2/:tran_id",
        component: PrintReceipt2Component
      }
    ]
  }
];
