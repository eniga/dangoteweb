import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/services.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-print-receipt',
  templateUrl: './print-receipt.component.html',
  styleUrls: ['./print-receipt.component.css']
})
export class PrintReceiptComponent implements OnInit {

  isLoading = true;
  error_msg = 'Loading...';
  has_error = false;

  barcode = '';
  tran_date: Date;
  cust_name = '';
  cust_address = '';
  country_code = '';
  region = '';
  teller_number = '';
  tran_id = '';
  comp_code = '';
  doc_num = '';
  fiscal_yr = '';
  status = '';
  amount = '';

  constructor(private http: ServicesService, private route: ActivatedRoute) {
    this.route.params.subscribe(res => this.tran_id = res.tran_id);
   }

  ngOnInit() {
    this.GetData();
  }

  GetData(){
    this.http.GetTransactionById(this.tran_id).subscribe((data) => {
      this.isLoading = false;
      if(data !== null){
        this.barcode = data.barcode1;
        this.tran_date = data.transaction_date;
        this.cust_name = data.cust_name;
        this.cust_address = data.receiver_address;
        this.country_code = data.receiver_country;
        this.region = data.receiver_region;
        this.teller_number = data.teller_no;
        this.comp_code = data.comp_code;
        this.doc_num = data.doc_num;
        this.fiscal_yr = data.fiscal_yr;
        this.status = data.status;
        this.amount = data.amount
      }
    }, error => {
      this.isLoading = false;
      this.has_error = true;
      this.error_msg = error.statusText;
    });
  }
}
