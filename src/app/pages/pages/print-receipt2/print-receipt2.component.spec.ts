import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintReceipt2Component } from './print-receipt2.component';

describe('PrintReceipt2Component', () => {
  let component: PrintReceipt2Component;
  let fixture: ComponentFixture<PrintReceipt2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintReceipt2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintReceipt2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
