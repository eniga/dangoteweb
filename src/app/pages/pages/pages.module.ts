import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { LoginComponent } from "./login/login.component";
import { LockComponent } from "./lock/lock.component";
import { RegisterComponent } from "./register/register.component";
import { PricingComponent } from "./pricing/pricing.component";
import { PagesRoutes } from "./pages.routing";
import { FormsModule } from "@angular/forms";
import { PrintReceiptComponent } from './print-receipt/print-receipt.component';
import { NgxBarcode6Module } from 'ngx-barcode6';
import { PrintReceipt2Component } from './print-receipt2/print-receipt2.component';

@NgModule({
  imports: [CommonModule, RouterModule.forChild(PagesRoutes), FormsModule, NgxBarcode6Module],
  declarations: [
    LoginComponent,
    LockComponent,
    RegisterComponent,
    PricingComponent,
    PrintReceiptComponent,
    PrintReceipt2Component
  ]
})
export class PagesModule {}
