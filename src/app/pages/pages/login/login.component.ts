import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { ServicesService } from "src/app/services.service";
import { LoginRequest } from "src/app/models";
import swal from 'sweetalert2';

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit, OnDestroy {
  public focus;
  public focus2;
  username: string = '';
  password: string = '';

  constructor(private router: Router, private service: ServicesService) { }

  ngOnInit() {
    var $page = document.getElementsByClassName("full-page")[0];
    var image_src;
    var image_container = document.createElement("div");
    image_container.classList.add("full-page-background");
    image_container.style.backgroundImage = "url(assets/img/bg16.jpg)";
    $page.appendChild(image_container);
    $page.classList.add("login-page");
    localStorage.removeItem('isLoggedin');
    localStorage.removeItem('user');
  }
  ngOnDestroy() {
    var $page = document.getElementsByClassName("full-page")[0];
    $page.classList.remove("login-page");
  }

  onLoggedin() {
    // localStorage.setItem('isLoggedin', 'true');
    // localStorage.setItem('user', 'testuser');
    // this.router.navigate(['/dashboard']);
    if(this.username.length < 1 || this.password.length < 1) {
      swal('Error', 'Please enter a username / password', 'error');
      return false;
    } else {
      let request = new LoginRequest;
      request.username = this.username;
      request.password = this.password;
      this.service.Login(request).subscribe((result) => {
        if(result !== null){
          localStorage.setItem('isLoggedin', 'true');
          localStorage.setItem('user', this.username);
          this.router.navigate(['/dashboard']);
        } else {
          swal('Error', 'Invalid username / password', 'error');
        }
      })

    } 
  }
}

