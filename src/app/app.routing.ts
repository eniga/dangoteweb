import { Routes } from "@angular/router";

import { AdminLayoutComponent } from "./layouts/admin-layout/admin-layout.component";
import { AuthLayoutComponent } from "./layouts/auth-layout/auth-layout.component";
import { AuthGuard } from "./auth.guard";

export const AppRoutes: Routes = [
  {
    path: "",
    redirectTo: "dashboard",
    canActivate: [AuthGuard],
    pathMatch: "full"
  },
  {
    path: "",
    component: AdminLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: "",
        loadChildren: "./pages/dashboard/dashboard.module#DashboardModule"
      },
      {
        path: "customers",
        loadChildren: "./pages/customers/customers.module#CustomersModule"
      },
      {
        path: "users",
        loadChildren: "./pages/users/users.module#UsersModule"
      },
      {
        path: "transactions",
        loadChildren: "./pages/transactions/transactions.module#TransactionsModule"
      },
      {
        path: "",
        loadChildren:
          "./pages/pages/user-profile/user-profile.module#UserModule"
      },
      {
        path: "",
        loadChildren: "./pages/pages/timeline/timeline.module#TimelineModule"
      }
    ]
  },
  {
    path: "",
    component: AuthLayoutComponent,
    children: [
      {
        path: "pages",
        loadChildren: "./pages/pages/pages.module#PagesModule"
      }
    ]
  }
];
