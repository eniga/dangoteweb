export class AuthenticationRequest {
    username: string;
    password: string;
    full_synch: string;
}

export class AuthenticationResponse {
    mt_full_auth_out: AuthResponse;
}

export class AuthResponse {
    authtoken: string;
    logged_on: string;
    error_log: ErrorLog[];
    records: AuthRecord[];
    regions: AuthRegion[];
    credit_areas: AuthCreditArea[];
}

export class ErrorLog {
    codeField: string;
    titleField: string;
    messageField: string;
}

export class AuthRecord {
    company_code: string;
    company_code_description: string;
    sales_organisation: string;
    sales_organisation_description: string;
    plant: string;
    plant_description: string;
    material: string;
    material_description: string;
    sales_unit: string;
    Base_unit: string;
    division: string;
}

export class AuthRegion {
    country: string;
    country_code: string;
    region_name: string;
}

export class AuthCreditArea {
    company_code: string;
    cbn_code: string;
    credit_area: string;
}

export class PasswordResetRequest {
    username: string;
    password: string;
    new_password: string;
    new_password_confirmation: string;
}

export class PasswordResetResponse {
    mt_pass_rest_out: ResetResponse;
}

export class ResetResponse {
    status: string;
    message: string;
    error_log: ErrorLog[];
}

export class CustomerDetailsRequest {
    cust_num: string;
    username: string;
}

export class CustomerDetailsResponse {
    mt_atm_cust_out: CustomerOutRespose;
}

export class CustomerOutRespose {
    cust_nameField: string;
    addressField: string;
    countryField: string;
    regionField: string;
    exposureField: string;
    divisionField: string;
    error_logField: ErrorLog[];
    access_tokenField: string;
}

export class PostPaymentRequest {
    access_token: string;
    comp_code: string;
    plant: string;
    bankn: string;
    cust_number: string;
    amount: string;
    teller_no: string;
    tran_id: string;
    currency: string;
    export: string;
    username: string;
    cust_name: string;
    account_no: string;
}

export class PostPaymentResponse {
    mt_bank_out: BankPayment;
}

export class BankPayment {
    eRRORField: ErrorLog[];
    output_string1Field: string;
    output_string2Field: string;
    docNumField: string;
    trans_idField: string;
    compCodeField: string;
    fiscalYrField: string;
    accessTokenField: string;
}

export class GetPriceRequest {
    delv_status: string;
    tran_id: string;
    sales_org: string;
    division: string;
    plant: string;
    cust_num: string;
    city: string;
    street: string;
    country: string;
    cust_name: string;
    region: string;
    export: string;
    materials: Materials[];
    quantities: Quantities2[];
    acess_token: string;
    username: string;
}

export class Materials {
    material_noField: string;
    material_txtField: string;
}

export class Quantities {
    qtyField: number;
}

export class Quantities2 {
    qtyField: number;
    qtyFieldSpeficied: boolean;
}

export class GetPriceResponse {
    mt_price_out: PriceResponse
}

export class PriceResponse {
    base_priceField: string;
    delivery_priceField: string;
    error_logField: ErrorLog[];
    access_tokenField: string;
}

export class CreateAtcRequest {
    cust_num: string;
    access_token: string;
    comp_code: string;
    bankn: string;
    amount: string;
    teller_no: string;
    sales_org: string;
    plant: string;
    split_type: string;
    delv_status: string;
    dest_region: string;
    receiver_name: string;
    dest_address: string;
    dest_city: string;
    dest_country: string;
    order_amount: string;
    tran_id: string;
    currency: string;
    export: string;
    material: Materials[];
    quantity: Quantities[];
    split_quantity: Quantities[];
    username: string;
    cust_name: string;
    account_no: string;
}

export class CreateAtcResponse {
    mt_doc_out: AtcResponse;
}

export class AtcResponse {
    transaction_statusField: TransactionStatus[];
    parentField: Parent[];
    childField: Parent[]
    paymentField: Parent[];
    barcode_string1Field: string;
    barcode_string2Field: string;
    access_tokenField: string;
    created_childrenField: string;
    errorField: ErrorLog[]
}

export class TransactionStatus {
    payment: string;
    atc: string;
}

export class Parent {
    docnum: string;
    doctype: string;
    info: string;
}

export class Areas {
    id: number;
    company_code: string;
    cbn_code: string;
    credit_area: string;
}

export class Records {
    id: number;
    company_code: string;
    sales_organisation: string;
    sales_organisation_description: string;
    plant: string;
    plant_description: string;
    material: string;
    material_description: string;
    sales_unit: string;
    base_unit: string;
    division: string;
}

export class Regions {
    id: number;
    country: string;
    country_code: string;
    region_name: string;
}

export class Transactions {
    tran_id: string;
    receiver_name: string;
    receiver_address: string;
    receiver_country: string;
    receiver_region: string;
    teller_no: string;
    comp_code: string;
    doc_num: string;
    atc_doc_number: string;
    order_status: string;
    fiscal_yr: string;
    status: string;
    amount: string;
    transaction_type: string;
    transaction_date: Date;
    processed_date: Date;
    barcode1: string;
    barcode2: string;
    username: string;
    cust_name: string;
}

export class LoginRequest {
    username: string;
    password: string;
}

export class LoginResponse {
    status: string;
    responseCode: string;
    data: UserData;
}

export class UserData {
    surname: string;
    givenName: string;
    email: string;
    name: string;
}

export class StaticData {
    cc_area: string;
    comp_code: string;
    sales_org: string;
}