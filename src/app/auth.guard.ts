import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { nextContext } from '@angular/core/src/render3';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {}

  // canActivate() {
  //   if (localStorage.getItem('isLoggedin') != null) {
  //     let result: number = +localStorage.getItem('isLoggedin');
  //     return result == 1 ? true : false;
  //   }

  //   this.router.navigate(['/pages/login']);
  //   return false;
  // }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if (localStorage.getItem('isLoggedin') != null) {
      return true;
    }

    // navigate to not found page
    this.router.navigate(['/pages/login']);
    return false;
  }
}
